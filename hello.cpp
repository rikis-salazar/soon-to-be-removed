#include <iostream>

// using namespace std;
// This ^^^ is not good! (specially if used in a library)

int main(){

    using namespace std;

    std::cout << "Hola, Mundo\n";
    cout << "Hello, World\n";
    std::cout << "Ciao, Mondo\n";

    return 0;
}


// Whatever...
// Whatever...
// Whatever...
// Whatever...
